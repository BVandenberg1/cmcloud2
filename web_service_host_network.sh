#!/bin/bash

vboxmanage natnetwork add --netname "nasp_cm_co"  --network 192.168.254.0 --dhcp off 

vboxmanage natnetwork modify --netname "nasp_cm_co" --port-forward-4 "Rule1:tcp:[]:50080:[192.168.254.10]:80" 
vboxmanage natnetwork modify --netname "nasp_cm_co" --port-forward-4 "Rule2:tcp:[]:50022:[192.168.254.10]:22"
vboxmanage natnetwork modify --netname "nasp_cm_co" --port-forward-4 "Rule3:tcp:[]:50443:[192.168.254.10]:443"

