

echo "Implementing auth keys file, installing packages, opening ports"

sudo mkdir /home/admin/.ssh/
sudo cp /cm_cloud/authorized_keys /home/admin/.ssh/authorized_keys
sudo yum install -y epel-release @core @base nmap-ncat vim git tcpdump
sudo firewall-cmd --permanent --zone=public --add-port=443/tcp
sudo firewall-cmd --permanent --zone=public --add-port=22/tcp
sudo firewall-cmd --permanent --zone=public --add-port=80/tcp
sudo firewall-cmd --reload
sudo yum install nginx -y

############################################################################################################

echo "nginx, MariaDB"

sudo systemctl start nginx
sudo systemctl enable nginx

sudo yum -y install mariadb server mariadb
sudo systemctl start mariadb
sudo mysql_secure_installation
mysql -u root -p <mariadb_security_config.sql
sudo systemctl enable mariadb


############################################################################################################

echo "php"
sudo yum install -y php php-mysql php-fpm
sudo cp /cm_cloud/php.ini /etc/php.ini
sudo cp /cm_cloud/www.conf /etc/php-fpm.d/www.conf
sudo systemctl start php-fpm
sudo systemctl enable php-fpm
sudo cp /cm_cloud/nginx.conf /etc/nginx/nginx.conf
sudo cp /cm_cloud/info.php /usr/share/nginx/html/info.php
sudo systemctl restart nginx

############################################################################################################

echo "Database config"

mysql -u root -p < wp_mariadb_config.sql

mysql -u root -e -p "SELECT user FROM mysql.user;"
mysql -u root -e -p "SHOW DATABASES;"

############################################################################################################

echo "Wordpress setup"

cd /
sudo wget http://wordpress.org/latest.tar.gz
sudo tar -xzvf latest.tar.gz
sudo cp /cm_cloud/wp-config.php wordpress/wp-config.php
sudo rsync -avP wordpress/ /usr/share/nginx/html
